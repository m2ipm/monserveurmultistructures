/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.universite.administratif;

/**
 *
 * @author PAKI6340
 */
public interface Const {
    
    static final String CHAR_SET="UTF-8";
    
    // HTTP return codes
    static final int HTTP_OK_STATUS = 200; //OK
    static final int HTTP_OK_CREATED = 201; //OK ressource créée
    static final int HTTP_OK_NOCONTENT = 204; //OK pour suppression resource
    static final int HTTP_KO_NOTALLOWED = 405; //Méthode non autorisée
    static final int HTTP_KO_NOTFOUND = 404; //Ressource non trouvée
    static final int HTTP_KO_BADREQUEST=400; //Requête incorrecte
    static final int HTTP_KO_NOTACCEPTABLE=406; //Le serveur ne peut pas fournir le mediatype demandé (enttete HTTP Accept)
    static final int HTTP_KO_UNSUPPORTEDMEDIA=415; //Le serveur ne peut pas accepter le mediatype fournit par la requête (entete HTTP Content-type) 
    static final int HTTP_REDIRECT = 302; // Redirect to the url specified in "Location" header
    
    // HTTP headers
    static final String ACCEPT = "Accept";
    static final String CONTENT = "Content-Type";
    static final String LOCATION = "Location";
}
