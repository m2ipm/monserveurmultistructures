/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.universite.administratif;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.universite.administratif.model.Etudiant;
import org.universite.administratif.model.EtudiantFactory;
import org.universite.intf.api.*;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 *
 * @author akriks
 */
public class EtudiantHandler implements HttpHandler, Const {

    /**
     * Gestion de la ressource Etudiants (/etudiants)
     *
     * @param httpE : Echnage HTTP encapsulant la requête et la réponse
     * @throws IOException
     */
    @Override
    public void handle(HttpExchange httpE) {

        System.out.println("Receive a call :" + httpE.getRequestURI());

        String methode = httpE.getRequestMethod().toUpperCase();
        if (methode.equals("GET")) {
            doGet(httpE);
        } else if (methode.equals("POST")) {
            doPost(httpE);
        } else if (methode.equals("DELETE")) {
            doDelete(httpE);
        } else {
            String response = "Ressource manipulable uniquement via GET/POST/DELETE ";
            envoiReponse(httpE, HTTP_KO_NOTALLOWED, response);
        }
    }

    //GET       
    private void doGet(HttpExchange httpE) {
        String[] pathParams = httpE.getRequestURI().getPath().split("/");
        String reponse;
        int status;
        //Test si présence d'infos après /etudiants dans l'url pour savoir si recherche de tous les étudiants ou d'un seul
        switch (pathParams.length) {
            case 2:
                List<Etudiant> etudiants = getAllEtudiants();
                reponse = getMappingListEtudiant(httpE, etudiants);
                status = HTTP_OK_STATUS;
                break;

            case 3:
                try {
                Etudiant etudiant = getEtudiantById(pathParams[2].trim());
                if (null == etudiant) {
                    status = HTTP_KO_NOTFOUND;
                    reponse = "Il n'existe pas d'�tudiant avec identifiant : " + pathParams[2].trim();
                } else {
                    status = HTTP_OK_STATUS;
                    reponse = getMappingEtudiant(httpE, etudiant);
                }
            } catch (NumberFormatException nfe) {
                status = HTTP_KO_BADREQUEST;
                reponse = "Requ�te incorrecte, identifiant �tudiant n'est pas numerique : " + pathParams[2].trim();

            }
            break;

            default:
                status = HTTP_KO_BADREQUEST;
                reponse = "Requ�te incorrecte, revoir l'url : " + httpE.getRequestURI().getPath();
                break;
        }
        envoiReponse(httpE, status, reponse);
    }

    private List<Etudiant> getAllEtudiants() {
        return EtudiantFactory.getEtudiants().values().stream().collect(toList());
    }

    private Etudiant getEtudiantById(String id) throws NumberFormatException {
        return EtudiantFactory.getEtudiants().get(Integer.valueOf(id));
    }

    // POST
    private void doPost(HttpExchange httpE) {
        String[] pathParams = httpE.getRequestURI().getPath().split("/");
        String reponse="";
        int status;
        //Test si présence d'infos après /etudiants dans l'url pour savoir si on a bien l'identifiant de l'�tudiant � supprimer
        switch (pathParams.length) {
            case 2:
                String body = getBody(httpE);                
                try {
                    MappingData map = getMapping(httpE, CONTENT);
                    EtudiantWs etudWs = (EtudiantWs) map.mapFrom(body, EtudiantWs.class);
                    Etudiant etudiant = ConvertData.toEtudiant(etudWs);
                    EtudiantFactory.getEtudiants().put(etudiant.getId(), etudiant);

                    // On redirige vers la ressource nouvellement cr��e
                    String url = httpE.getRequestURI().getPath();
                    if (!url.endsWith("/")) {
                        url = url.concat("/");
                    }
                    url = url.concat(String.valueOf(etudiant.getId()));
                    httpE.getResponseHeaders().add(LOCATION, url);
                    status= HTTP_REDIRECT;
                } catch (IOException ex) {
                    status = HTTP_KO_BADREQUEST;
                    reponse = ex.getMessage();
                }
                break;

            default:
                status = HTTP_KO_BADREQUEST;
                reponse = "Requ�te incorrecte pour un POST, revoir l'url : " + httpE.getRequestURI().getPath();
                break;
        }
        envoiReponse(httpE, status, reponse);            
    }


    //DELETE       
    private void doDelete(HttpExchange httpE) {
        String[] pathParams = httpE.getRequestURI().getPath().split("/");
        String reponse;
        int status;
        //Test si présence d'infos après /etudiants dans l'url pour savoir si on a bien l'identifiant de l'�tudiant � supprimer
        switch (pathParams.length) {
            case 3:
                try {
                    if (deleteEtudiantById(pathParams[2].trim())) {
                        status = HTTP_OK_NOCONTENT;
                        reponse = "";
                    } else {
                        status = HTTP_KO_NOTFOUND;
                        reponse = "Il n'existe pas d'�tudiant avec identifiant : " + pathParams[2].trim();
                    }
                } catch (NumberFormatException nfe) {
                    status = HTTP_KO_BADREQUEST;
                    reponse = "Requ�te incorrecte, identifiant �tudiant n'est pas numerique : " + pathParams[2].trim();

                }
                break;

            default:
                status = HTTP_KO_BADREQUEST;
                reponse = "Requ�te incorrecte, revoir l'url : " + httpE.getRequestURI().getPath();
                break;
        }
        envoiReponse(httpE, status, reponse);
    }

    private boolean deleteEtudiantById(String id) throws NumberFormatException {
        int del = Integer.valueOf(id);
        if (EtudiantFactory.getEtudiants().containsKey(del)) {
            EtudiantFactory.getEtudiants().remove(del);
            return true;
        } else {
            return false;
        }
    }

    private String getBody(HttpExchange httpE) {
        StringBuilder reqBody = new StringBuilder();
        String line;
        InputStream is = httpE.getRequestBody();
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        try {
            while ((line = in.readLine()) != null) {
                reqBody.append(line);
            }
        } catch (IOException ex) {
            System.out.println("Erreur de lecture du corps de message");
        }
        return reqBody.toString();
    }

    private String getMappingListEtudiant(HttpExchange httpE, List<Etudiant> etudiants) {
        EtudiantListWs listEtudWs = new EtudiantListWs();
        listEtudWs.getEtudiants().addAll((etudiants.stream()
                .map(etud -> ConvertData.toEtudiantWs(etud))
                .collect(Collectors.toList())));
        MappingData map = getMapping(httpE, ACCEPT);
        return map.mapTo(listEtudWs);
    }

    private String getMappingEtudiant(HttpExchange httpE, Etudiant etudiant) {
        //EtudiantListWs listEtudWs = new EtudiantListWs();
        EtudiantWs etuWs = ConvertData.toEtudiantWs(etudiant);
        MappingData map = getMapping(httpE, ACCEPT);
        return map.mapTo(etuWs);
    }

    private MappingData getMapping(HttpExchange httpE, String header) {
        MappingData map = new MappingCSV();
        List<String> acceptHeaders = httpE.getRequestHeaders().getOrDefault(header, null);
        if (null == acceptHeaders || acceptHeaders.contains("text/csv")) {
            map = new MappingCSV();
        } else if (acceptHeaders.contains("application/xml")) {
            map = new MappingXML();
        } else if (acceptHeaders.contains("application/json")) {
            map = new MappingJSON();
        }
        httpE.getResponseHeaders().add(CONTENT, map.contentType());
        return map;
    }

    private void envoiReponse(HttpExchange httpE, int status, String reponse) {
        try {
            httpE.sendResponseHeaders(status, reponse.getBytes(CHAR_SET).length);
            OutputStream os = httpE.getResponseBody();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            osw.write(reponse);
            osw.close();
        } catch (IOException ioe) {
            System.out.println("Erreur d'ecriture de la r�ponse");
        }
    }

}
