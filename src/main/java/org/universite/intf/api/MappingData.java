/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.universite.intf.api;

import java.io.IOException;

/**
 *
 * @author akriks
 */
public interface MappingData {

    public String contentType();
    public String mapTo(Object obj);
    public Object mapFrom(String texte, Class classe) throws IOException;
    
}
