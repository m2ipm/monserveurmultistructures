/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.universite.intf.api;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author akriks
 */
public class MappingXML implements MappingData {

    private JAXBContext contextObj;
    private final String CONTENT_TYPE = "application/xml; charset=UTF-8";

    @Override
    public String contentType() {
        return CONTENT_TYPE;
    }

    public MappingXML() {
        try {
            contextObj = JAXBContext.newInstance("org.universite.intf.api");

        } catch (JAXBException ex) {
            Logger.getLogger(MappingXML.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String mapTo(Object obj) {
        OutputStream os = new ByteArrayOutputStream();
        try {
            Marshaller marshallerObj = contextObj.createMarshaller();
            marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshallerObj.marshal(obj, os);
        } catch (JAXBException ex) {
            Logger.getLogger(MappingXML.class.getName()).log(Level.SEVERE, null, ex);
        }
        return os.toString();
    }

    @Override
    public Object mapFrom(String texte, Class classe) throws IOException {
        Unmarshaller jaxbUnmarshaller;  
        try {
            jaxbUnmarshaller = contextObj.createUnmarshaller();
            return jaxbUnmarshaller.unmarshal(new StringReader(texte)); 
        } catch (JAXBException ex) {
            throw new IOException("Impossible de convertir le message XML");
        }
    }
}
