/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.universite.intf.api;

import org.universite.administratif.model.Etudiant;
import org.universite.administratif.model.EtudiantFactory;

import java.math.BigInteger;

/**
 *
 * @author akriks
 */
public class ConvertData {
    
    /**
     * Convertit un Etudiant de l'API en classe Etudiant du domaine m�tier
     * @param etudws
     * @return
     */
    public static Etudiant toEtudiant(EtudiantWs etudws)  {
        Etudiant etud=new Etudiant();
        if (null==etudws.getId())
            etudws.setId(newid());
       
        etud.setId(etudws.getId().intValue());
        etud.setNom(etudws.getNom());
        etud.setPrenom(etudws.getPrenom());
        return etud;
    }
    
    /**
     * Convertit Etudiant du domaine m�tier  en classe un Etudiant de l'API
     * @param etud
     * @return
     */
    public static EtudiantWs toEtudiantWs(Etudiant etud) {
        EtudiantWs etudws=new EtudiantWs();
        etudws.setId(BigInteger.valueOf(etud.getId()));
        etudws.setNom(etud.getNom());
        etudws.setPrenom(etud.getPrenom());
        return etudws;
        
    }

    private static BigInteger newid() {
        int maxid= EtudiantFactory.getEtudiants().keySet()
                                                        .stream()
                                                        .mapToInt(v -> v)
                                                        .max().orElse(Integer.MIN_VALUE);
        return BigInteger.valueOf(maxid+1);
    }
}
