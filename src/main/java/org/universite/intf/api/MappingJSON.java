/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.universite.intf.api;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author akriks
 */
public class MappingJSON implements MappingData  {

    private final String CONTENT_TYPE="application/json; charset=UTF-8";

    @Override
    public String contentType() {
       return CONTENT_TYPE;
    }
    
    @Override
    public String mapTo(Object obj) {
            OutputStream os=new ByteArrayOutputStream();
            ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(os, obj);
        } catch (IOException ex) {
            Logger.getLogger(MappingJSON.class.getName()).log(Level.SEVERE, null, ex);
        }
            return os.toString();
        }


    @Override
    @SuppressWarnings("unchecked")
    public Object  mapFrom(String texte, Class classe) throws IOException {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(texte, classe);
    }
}
