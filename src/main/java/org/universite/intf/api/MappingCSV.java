/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.universite.intf.api;

import java.io.IOException;
import java.util.StringTokenizer;

/**
 *
 * @author akriks
 */
public class MappingCSV implements MappingData  {
        
    private final String CONTENT_TYPE="text/csv; charset=UTF-8";
    private final String SEP=";";
    
    @Override
    public String contentType() {
       return CONTENT_TYPE;
    }
    
        
    @Override
    public String mapTo(Object obj) {
        String mappingCSV="";
        if (obj instanceof EtudiantListWs ) {
            mappingCSV=mapTo((EtudiantListWs)obj);
        } else if (obj instanceof EtudiantWs) {
            mappingCSV=mapTo((EtudiantWs) obj);
        } else {
            mappingCSV="Pas de serialisation CSV disponible pour cet objet."; 
        }
        return mappingCSV;
    }

        
        public String mapTo(EtudiantListWs etudiants) {
            // on ne fait pour l'instant que le mapping d'une liste d'�tudiants
            StringBuilder reponse = new StringBuilder();
            etudiants.getEtudiants().forEach((EtudiantWs e) -> reponse.append(mapTo(e)));
            return reponse.toString();
        }
        
        public String mapTo(EtudiantWs etudiant) {
            return new StringBuilder().append(etudiant.getId())
                                      .append(SEP)
                                      .append(etudiant.getPrenom())
                                      .append(SEP)
                                      .append(etudiant.getNom())
                                      .append(SEP)
                                      .append("\r\n")
                                      .toString();
        }

    @Override
    public Object mapFrom(String texte, Class classe) throws IOException {
      EtudiantWs etudWs=new EtudiantWs();
        if (!classe.equals(EtudiantWs.class)) {
            throw new IOException("Uniquement classe Etudiant prise en charge en CSV");
        } else {
            StringTokenizer st=new StringTokenizer(texte,";");
            if  ((null==st)  || (st.countTokens()!=2)) {
                throw new IOException("Impossible de convertir le message en CSV");
            }
            etudWs.setPrenom(st.nextToken().split("=")[1]);
            etudWs.setNom(st.nextToken().split("=")[1]);
        }
        return etudWs;
    }
}
