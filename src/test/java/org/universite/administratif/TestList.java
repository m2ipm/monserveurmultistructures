/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.universite.administratif;

import org.universite.administratif.model.Etudiant;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author akriks
 */
public class TestList {
    static List<Etudiant> list= new ArrayList<>();

    public static void main(String[] args) {
        list.add(new Etudiant().setId(233).setNom("Durand").setPrenom("Jean"));
        list.add(new Etudiant().setId(399).setNom("Feng").setPrenom("Shuaining"));
        list.add(new Etudiant().setId(45).setNom("Picasso").setPrenom("Pablo"));
        
        System.out.println("index--------------------------------------");
        for (int i=0; i<list.size();i++) {
            System.out.println (list.get(i));
            // si on cherche un �tudiant dont le nom est "Picasso" alors on fait ce test
            if (list.get(i).getNom().equals("Picasso")) {
                System.out.println("Trouvé");
            }
                  
        }
        System.out.println("Iterator--------------------------------------");
        Iterator it=list.iterator();
        Etudiant etu;
        while(it.hasNext()) {
            etu= (Etudiant)it.next();
            System.out.println(etu);
            // si on cherche un �tudiant dont le nom est "Picasso" alors on fait ce test
            if (etu.getNom().equals("Picasso")) {
                System.out.println("Trouvé");
            }
        }
        
        System.out.println("Parcours des objets avec for------------------------------");
        for (Etudiant etu1: list) {
             System.out.println(etu1);
            // si on cherche un �tudiant dont le nom est "Picasso" alors on fait ce test
            if (etu1.getNom().equals("Picasso")) {
                System.out.println("Trouvé");
            }
        }
        
        System.out.println("Parcours via forEach --------------------------------------");
        list.forEach(etu2 -> { System.out.println(etu2);
                                if (etu2.getNom().equals("Picasso")) {
                                        System.out.println("Trouvé");
                                }
                             });
        
        System.out.println("Utilisation de stream --------------------------------------");
        list.stream().anyMatch(etu3 -> etu3.getNom().equals("Picasso")); // retourne true si au moins 1 nom est egal � Picasso
        list.stream().allMatch(etu3 -> etu3.getNom().equals("Picasso")); // retourne true si tous les noms sont �gaux � Picasso
        List newList=list.stream().map(etu3 -> etu3.setNom(etu3.getNom().toUpperCase())).collect(Collectors.toList());  // on fait un mapping pour retourner une liste mais avec tous les noms en majuscule  
        newList.forEach(etu4 -> System.out.println(etu4));
    }
}
